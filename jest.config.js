module.exports = {
  transform: {
    "^.+\\.ts$": "ts-jest"
  },
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "abnf"],
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.[jt]sx?$",
  testMatch: undefined,
  testEnvironment: "node",
  preset: "ts-jest",
  globals: {
    "ts-jest": {
      tsConfig: "./tsconfig.test.json"
    }
  },
  roots: ["<rootDir>/packages"]
}