import { NextApiRequest, NextApiResponse } from "next"
import fs from "fs"
import path from "path"
import url from "url"
import ejs from "ejs"
import env from "../../common/env"

export default (req: NextApiRequest, res: NextApiResponse) => {
  if (req.url) {
    const xml = fs.readFileSync(
      path.join(
        path.dirname(path.join(process.cwd(), "src/pages", req.url)),
        "voice.xml"
      ),
      "utf8"
    )
    res.writeHead(200, {
      "Content-Type": "application/xml"
    })
    res.end(
      ejs.render(xml, {
        method: { gather: "POST" },
        action: { gather: env.routes.api.gather }
      })
    )
  } else {
    res.status(404)
  }
}