FROM node:alpine as artifacts

# Install basic requirements
RUN apk add --update --no-cache nodejs-current yarn

# Pull the project repository
RUN apk add --update --no-cache --virtual .tmp git
WORKDIR /root
RUN git clone --depth 1 https://gitlab.com/yuhr/doorphone.home.git

# Populate dependencies
RUN apk add --no-cache --virtual .tmp python make g++
WORKDIR /root/doorphone.home
RUN yarn install --production --frozen-lockfile --network-timeout 600000 --registry https://registry.npmjs.org
RUN yarn lerna bootstrap --production

# Clean up
RUN apk del .tmp

ENTRYPOINT [ "yarn", "next" ]