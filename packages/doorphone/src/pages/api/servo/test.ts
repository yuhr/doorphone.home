import { Board, Servo } from "johnny-five"
import { RaspiIO } from "raspi-io"
const board = new Board({ io: new RaspiIO() })

import { NextApiRequest, NextApiResponse } from "next"
export default (req: NextApiRequest, res: NextApiResponse) => {
  board.on("ready", () => {
    const servo = new Servo("GPIO4")
    servo.sweep({
      range: [45, 135],
      interval: 10000
    })
  })
}