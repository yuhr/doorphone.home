import doorphone from "./"

const queries: string[] = ["test"]
queries.forEach(query => {
  test(`Search with ${JSON.stringify(query)}`, async () => {
    await doorphone.call()
    expect(query).toBeDefined()
  })
})