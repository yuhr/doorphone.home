import url from "url"
const env = {
  URL: process.env.URL || "http://localhost:3000"
}
const routes = {
  api: {
    voice: url.resolve(env.URL, "/api/voice"),
    gather: url.resolve(env.URL, "/api/gather")
  }
}
export default { ...env, routes }