import { NextApiRequest, NextApiResponse } from "next"

export default (req: NextApiRequest, res: NextApiResponse) => {
  if (req.url) {
    console.log(JSON.stringify(req.headers))
    console.log(JSON.stringify(req.body))
    res.status(200).send("")
  } else {
    res.status(404)
  }
}