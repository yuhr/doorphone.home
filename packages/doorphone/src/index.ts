import runtypes from "runtypes"
import twilio from "twilio"
import url from "url"
import env from "./common/env"

export const call = async () => {
  const accountSid = "AC0a1ebbdec26eec969a7ebdfdbf07b633"
  const authToken = "7527bf123b249c101aa4d879cf6fd212"
  const client = twilio(accountSid, authToken)
  const call = await client.calls.create({
    url: env.routes.api.voice,
    to: "+818093039605",
    from: "+17203308768"
  })
  console.log(call)
}

export const transfer = async () => {}

export default { call }